import { useState, useEffect} from 'react'

function ManufacturersList() {
  const [manufacturers, setManufacturers] = useState([])

  const getData = async () => {
    const resp = await fetch('http://localhost:8100/api/manufacturers/');

    if (resp.ok) {
      const data = await resp.json();
      console.log(data)
      setManufacturers(data.manufacturers)
    }
  }

  useEffect(()=>{
    getData()
  }, [])

  return (
    <>
    <div>
      <h1 className="display-5 fw-bold">Manufacturers</h1>
    </div>
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Name</th>
        </tr>
      </thead>
      <tbody>
        {manufacturers.map(manufacturer => {
          return (
            <tr>
              <td>{manufacturer.name}</td>
            </tr>
          );
        })}
      </tbody>
    </table>
    </>
  );
}

export default ManufacturersList;
