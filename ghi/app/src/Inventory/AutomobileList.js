import { useState, useEffect} from 'react'

function AutomobileList() {
  const [autos, setAutomobile] = useState([])


  const getData = async () => {
    const resp = await fetch('http://localhost:8100/api/automobiles/');

    if (resp.ok) {
      const data = await resp.json();
      console.log(data)
      setAutomobile(data.autos)
    }
  }

  useEffect(()=>{
    getData()
  }, [])

  return (
    <>
    <div>
      <h1 className="display-5 fw-bold">Vehicle models</h1>
    </div>
    <table className="table table-striped">
      <thead>
        <tr>
          <th>VIN</th>
          <th>Color</th>
          <th>Year</th>
          <th>Model</th>
          <th>Manufacturer</th>
        </tr>
      </thead>
      <tbody>
        {autos.map(auto => {
          return (
            <tr>
              <td>{auto.vin}</td>
              <td>{auto.color}</td>
              <td>{auto.year}</td>
              <td>{auto.model}</td>
              <td>{auto.manufacturer}</td>
            </tr>
          );
        })}
      </tbody>
    </table>
  </>
  );
}

export default AutomobileList;
