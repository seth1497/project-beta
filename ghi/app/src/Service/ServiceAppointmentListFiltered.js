import { useState, useEffect} from 'react'

function ServiceAppointmentListFiltered() {
    const [appointments, setServiceAppointments] = useState([])
    const [filterValue, setFilter] = useState([])

    const getData = async () => {
        const resp = await fetch('http://localhost:8080/api/service_rest/')

        if (resp.ok) {
            const data = await resp.json()
            console.log(data);
            setServiceAppointments(data.appointments)
        }
    }


    const handleChange = (event) => {
        setFilter(event.target.value)
    }
    let filteredAppointments = [];
    if (filterValue === "") {
        filteredAppointments = appointments;
    }
    else {
        filteredAppointments = appointments.filter((appointment) =>
            appointment.automobile === filterValue
        );
    }

    useEffect(()=> {
        getData()
    }, [])


    return <>
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <div>
                        <input className="form-control" value={filterValue} onChange={handleChange} placeholder="Search VIN" />
                    </div>
                    <h1>Service Appointments</h1>
                    <table className="table table-striped">
                        <thead>
                            <tr>
                                {/* <th>id</th> */}
                                <th>VIN</th>
                                <th>Customer name</th>
                                <th>Date</th>
                                <th>Time</th>
                                <th>Technician</th>
                                <th>Reason</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>

                        <tbody>
                            {
                                filteredAppointments.map(appointment => {
                                    return (
                                    <tr key={ appointment.id }>
                                        {/* <td>{ appointment.id }</td> */}
                                        <td>{ appointment.automobile }</td>
                                        <td>{ appointment.customer_name }</td>
                                        <td>{ appointment.date }</td>
                                        <td>{ appointment.time }</td>
                                        <td>{ appointment.technician.name }</td>
                                        <td>{ appointment.reason }</td>
                                    </tr>
                                    );
                                })
                            }
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </>
}

export default ServiceAppointmentListFiltered;
