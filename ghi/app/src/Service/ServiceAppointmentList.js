// import { Link } from 'react-router-dom'
import { useState, useEffect} from 'react'

const ServiceAppointmentList = () => {
    const [appointments, setServiceAppointments] = useState([])

    const getData = async () => {
        const resp = await fetch('http://localhost:8080/api/service_rest/')

        if (resp.ok) {
            const data = await resp.json()
            console.log(data);
            setServiceAppointments(data.appointments)
        }
    }

    useEffect(()=> {
        getData()
    }, [])



    const handleDelete = async (e) => {
        const url = `http://localhost:8080/api/service_rest/${e.target.id}`

        const fetchConfigs = {
            method: "Delete",
            headers: {
                "Content-Type": "application/json"
            }
        }

        const resp = await fetch(url, fetchConfigs)
        const data = await resp.json()


        setServiceAppointments(appointments.filter(appointment=> String(appointment.id) !== e.target.id))
        // getData()
    }

    return <>
        <h1>Service Appointments</h1>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>id</th>
                    <th>VIN</th>
                    <th>Customer name</th>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Technician</th>
                    <th>Reason</th>
                    <th>VIP</th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>

            <tbody>
                {
                    appointments.map(appointment => {
                        return (
                        <tr key={appointment.href}>
                            <td>{ appointment.id }</td>
                            <td>{ appointment.automobile }</td>
                            <td>{ appointment.customer_name }</td>
                            <td>{ appointment.date }</td>
                            <td>{ appointment.time }</td>
                            <td>{ appointment.technician.name }</td>
                            <td>{ appointment.reason }</td>
                            <td>{ appointment.vip.toString() }</td>
                            <td><button onClick={handleDelete} id={appointment.id} className="btn btn-danger">Cancel</button></td>
                            <td><button onClick={handleDelete} id={appointment.id} className="btn btn-success">Finished</button></td>
                        </tr>
                        );
                    })
                }
            </tbody>
        </table>
    </>
}

export default ServiceAppointmentList;
