import React, {useState, useEffect} from 'react';

function CustomerForm() {
  const [name, setName] = useState('')
  const [address, setAddress] = useState('')
  const [phone_number, setPhoneNumber] = useState('')


  const fetchData = async () => {
    const url = 'http://localhost:8090/api/customers/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
    }
  }

  useEffect(() => {
    fetchData();
}, [])

const handleNameChange = (e) => {
    setName(e.target.value);
};
const handleAddressChange = (e) => {
    setAddress(e.target.value);
};
const handlePhoneNumberChange = (e) => {
    setPhoneNumber(e.target.value);
};

const handleSubmit = async (e) => {
    e.preventDefault();

    const data = {
      "name": name,
      "address": address,
      "phone_number": phone_number,
    }

    const customerURL = 'http://localhost:8090/api/customers/'
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(customerURL, fetchConfig);

    if (response.ok) {
      const newCustomer = await response.json();
      console.log(newCustomer)

     setName('');
     setAddress('');
     setPhoneNumber('');
  }
}

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add a potential customer</h1>
          <form onSubmit={handleSubmit} id="add-customer-form">
            <div className="form-floating mb-3">
              <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
              <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleAddressChange} value={address} placeholder="address" required type="text" name="address" id="address" className="form-control" />
              <label htmlFor="address">Address</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handlePhoneNumberChange} value={phone_number} placeholder="phone_number" required type="text" name="phone_number" id="phone_number" className="form-control" />
              <label htmlFor="phone_number">Phone Number</label>
            </div>
            <button className="btn btn-primary">Add</button>
          </form>
        </div>
      </div>
    </div>

  );
}

export default CustomerForm;
