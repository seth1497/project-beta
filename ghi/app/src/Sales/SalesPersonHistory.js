import React, { useState, useEffect } from "react";

function SalesPersonHistory() {
    const [sales, setSales] = useState([]);
    const [filterValue, setFilter] = useState("");

  const getData = async () => {
    const resp = await fetch("http://localhost:8090/api/sales/");

    if (resp.ok) {
      const data = await resp.json();
      const sales = data.sales.map((person) => {
        return {
          sales_person: person.sales_person.name,
          customer: person.customer.name,
          VIN: person.automobile.vin,
          sales_price:person.price,
        };
      });

      setSales(sales);
    }
  }

  const handleChange = (event) => {
    setFilter(event.target.value)
  }
  let filteredSales = [];
  if (filterValue === ""){
    filteredSales = sales;
  } else {
        filteredSales = sales.filter((sale) =>
            sale.sales_person === filterValue
        );
  }


  useEffect(() => {
    getData()},[])

  return <>
        <div className = "row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <div>
                        <select value={filterValue} onChange={handleChange} placeholder="Select a salesperson" className="form-select">
                    <option value="">Select a salesperson</option>
                    {sales.map(person=> {
                        return (
                          <option key={person.sales_person} value={person.sales_person}>{person.sales_person}</option>
                        )
                    })}
                </select>
                    </div>
        <h1>Sales person history</h1>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Sale person</th>
                    <th>Customer</th>
                    <th>VIN</th>
                    <th>Sales price</th>
                </tr>
            </thead>
            <tbody>
                {
                    filteredSales.map(sale => {
                        return (
                        <tr key={ sale.VIN }>
                            <td>{sale.sales_person}</td>
                            <td>{ sale.customer }</td>
                            <td>{ sale.VIN }</td>
                            <td>${ sale.sales_price }</td>
                        </tr>
                        )
                    })
                }
            </tbody>
        </table>
    </div>
    </div>
    </div>
    </>
}
export default SalesPersonHistory;
