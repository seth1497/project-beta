import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import SalesPersonForm from './Sales/SalesPersonForm';
import CustomerForm from './Sales/CustomerForm'
import SalesRecordForm from './Sales/SalesRecord';
import SalesList from './Sales/SalesList'
import ManufacturersList from './Inventory/ManufacturersList';
import ManufacturerForm from './Inventory/ManufacturerForm';
import ModelsList from './Inventory/ModelsList';
import ModelForm from './Inventory/ModelForm';
import AutomobileList from './Inventory/AutomobileList';
import TechnicianForm from './Service/TechnicianForm';
import ServiceAppointmentForm from './Service/ServiceAppointmentForm';
import ServiceAppointmentList from './Service/ServiceAppointmentList'
import SalesPersonHistory from './Sales/SalesPersonHistory';
import AutomobileForm from './Inventory/AutomobileForm';
import ServiceAppointmentListFiltered from './Service/ServiceAppointmentListFiltered';


function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="manufacturers">
            <Route index element={<ManufacturersList/>}/>
            <Route path="new" element={<ManufacturerForm/>}/>
          </Route>
          <Route path="models" element={<ModelsList/>}/>
          <Route path='models/new' element={<ModelForm/>}/>
          <Route path="automobiles" element={<AutomobileList/>}/>
          <Route path ="salesperson/new" element={<SalesPersonForm/>}/>
          <Route path = "customer/new" element={<CustomerForm/>}/>
          <Route path = "sales" element={<SalesList/>}/>
          <Route path="sales/new" element= {<SalesRecordForm/>}/>
          <Route path="sales/history" element= {<SalesPersonHistory/>}/>
          <Route path = "technician/new" element = {<TechnicianForm/>}/>
          <Route path = "serviceappointment" element = {<ServiceAppointmentList/>}/>
          <Route path = "serviceappointment/new" element = {<ServiceAppointmentForm/>}/>
          <Route path = "serviceappointmentfiltered" element = {<ServiceAppointmentListFiltered/>}/>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
