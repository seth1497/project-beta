from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    vin = models.CharField(max_length=17, unique=True)

    def get_api_url(self):
        return reverse("api_automobile", kwargs={"pk": self.id})

class Technician(models.Model):
    name = models.CharField(max_length=200, unique=True)
    employee_number = models.PositiveSmallIntegerField(unique=True)

class ServiceAppointment(models.Model):
    automobile = models.CharField(max_length=17)
    customer_name = models.CharField(max_length=200)
    date = models.DateField(blank=True, null=True)
    time = models.TimeField(blank=True, null=True)
    technician = models.ForeignKey(
        Technician,
        related_name="service_appointments",
        on_delete=models.CASCADE,
    )
    reason = models.CharField(max_length=200)
    vip = models.BooleanField(default=False)
