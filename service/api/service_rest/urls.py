from django.urls import path

from .views import api_list_appointments, api_list_technicians, api_service_history, api_show_appointment, api_show_technician

urlpatterns = [
    path("service_rest/", api_list_appointments, name="api_list_appointments"),
    path("technicians/", api_list_technicians, name="api_list_technicians"),
    path("technicians/<int:id>/", api_show_technician, name="api_show_technicians"),
    path("service_rest/<int:id>/", api_show_appointment, name="api_show_appointment"),
    path("service_rest/servicehistory/<str:vin>/", api_service_history, name="api_service_history"),
]
