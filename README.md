# CarCar

Team:

* Seth - Service
* Paula - Sales

## Design

## Service microservice

The Service microservice is composed of an AutomobileVO model, a Technician model, and a ServiceAppointment model which includes two foreign keys to the AutomobileVO model and the Technician model.

The AutomobileVO polls the Inventory microservice to create the value object instances for automobile from the automobile instances in the Inventory microservice which is outside the bounded context of the Service microservice, running in a different container.


## Sales microservice

The Sales microservice is composed of an AutomobileVO model, a SalesPerson model, a Customer model and a SalesRecord model which includes three foreign keys to the exisiting models.

The AutomobileVO polls the Inventory microservice to create the value object instances for automobile from the automobile instances in the Inventory microservice which is outside the bounded context of the Sales microservice, running in a different container.
